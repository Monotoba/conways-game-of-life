

class GameOfLife {
 
  int w = 16;
  int columns, rows;
  
  Cell[][] board;
  
  GameOfLife() {
   columns = width/w;
   rows = height/w;
   board = new Cell[columns][rows];
   init();
  }
  
  // Initialize board
  void init() {
   for(int i = 0; i < columns; i++) {
     for(int j = 0; j < rows; j++) {
       board[i][j] = new Cell(i*w, j*w, w); 
     }
   }
  }
  
  // Generate cells states
  void generate() {
   for(int i = 0; i < columns; i++) {
    for(int j = 0; j < rows; j++) {
     board[i][j].savePreviousState(); 
    }
   }
   
   // Loop over every cell in our 2D array and check it's neighbors
   for(int x = 0; x < columns; x++) {
    for(int y = 0; y < rows; y++) {
      
     // Add up the states in all nieghtbors (the 3x3 grid surrounding our current location)
     int neighbors = 0;
     for(int i = -1; i <= 1; i++) {
       for(int j = -1; j <= 1; j++) {
        neighbors += board[(x+i+columns)%columns][(y+j+rows)%rows].previous_state; 
       }
     }
     
     // Subtract the current cells state since we added it to nieghbors in the above loop
     neighbors -= board[x][y].previous_state;
     
     // Game rules applied
     if((board[x][y].state == 1) && (neighbors < 2)) board[x][y].newState(0);
     else if((board[x][y].state == 1) && (neighbors > 3)) board[x][y].newState(0);
     else if ((board[x][y].state == 0) && (neighbors == 3)) board[x][y].newState(1);
     // otherwise remain in stacies. 
    }
   }
  }
  
  // Display board
  void show() {
   for(int i = 0; i < columns; i++) {
    for(int j = 0; j < rows; j++) {
      board[i][j].show(); 
    }
   }
  } 
} // End Class
