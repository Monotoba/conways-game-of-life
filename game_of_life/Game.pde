
GameOfLife game;

void settings() {
  size(640, 480);
}
void setup() {
  //size(640, 480);
 game = new GameOfLife();
}

void draw() {
 background(255);
 
 game.generate();
 game.show();
}

// Reset our game board when mouse is pressed
void mousePressed() {
  game.init();
}
