
class Cell {
  float x, y;
  float w;
  
  color birthColor = color(0x46, 0xff, 0x0);
  color livingColor = color(0x4b, 0x83, 0x0d);
  color dyingColor = color(0x00, 0x33, 0x00);
  color emptyColor = color(255, 255, 255);
  
  
  
  int state;
  int previous_state;
  
  Cell(float x, float y, float w) {
    this.x = x;
    this.y = y;
    this.w = w;
    
    this.state = int(random(2));
    this.previous_state = this.state;
  }
  
  void savePreviousState() {
    this.previous_state = this.state;
  }
  
  void newState(int state) {
    this.state = state;
  }
  
  void show() {
   if(this.previous_state == 0 && this.state == 1) fill(birthColor);
   else if (this.state == 1) fill(livingColor);
   else if(this.previous_state == 1 && state == 0) fill(dyingColor);
   else fill(emptyColor);
   stroke(0);
   rect(this.x, this.y, this.w, this.w);
  }
    
}
